﻿#include <iostream>
#include <cmath>

class Vector
{
public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
		double sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
		std::cout << "\n" << sqrt;
		std::cout << "\n" << y;
	}
private:
	double x;
	double y;
	double z;
};

class hp
{
private:
	int a;
public:
	int GetA()
	{
		return a;
	}

	void SetA(int newA)
	{
		a = newA;
	}
};

int main()
{
	hp temp, temp1;
	temp.SetA(100);
	temp1.SetA(99);
	std::cout<<temp.GetA() << ' ' << temp1.GetA();

	Vector v(2, 4, 6);
	v.Show();

}